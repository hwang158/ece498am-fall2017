This repository is for [ECE/CS 498AM Applied Cryptography](http://soc1024.ece.illinois.edu/teaching/ece498am/fall2017/
) at University of Illinois at Urbana-Champaign

These files are in python. They should work under EWS linux instances (linux.ews.illinois.edu)


- mp1:
This code uses Jeremy Kun's beautiful [python library for elliptic curves and finite fields](https://github.com/j2kun/elliptic-curves-finite-fields). Alternatives like `gmpy` would be feasible as well.


- mp2: to release
